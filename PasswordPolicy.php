<?php

//namespace BSG;

/**
 * ServicesContainer interface
 *
 * @package default
 * @author Paul McIntosh
 */
interface ServicesContainer {

	public function addService(Services $item);


}

/**
 * Service class implements ServicesContainer
 *
 * @package default
 */
class Service implements ServicesContainer {

	public $items;
	
	public function addService(Services $item) {

		$this -> items[] = $item;

	}

}

/**
 * Services Interface
 *
 * @package default
 */
interface Services {

	public function isAllowed($it);


}

/**
 * Serve abstract class implements Services
 *
 * @package default
 */
abstract class Serve implements Services {

	protected $service;

	public $type;

	public function __construct($type, $service = false) {

		$this -> type = $type;

	}


	public function isAllowed($it) {
				
		print_r($it->current());
		
	}

}

/**
 * StartService class
 *
 * @package default
 */
class StartServiceExculsions extends Serve {

	public function __construct($passwords,$exculsions){

			
		$it = new ArrayIterator($passwords[0]->type);
		
		$it = new CachingIterator($it);
		
		foreach ($it as $el) {
			
			if($it->hasNext()){
								
				$this->isAllowed($it);
				
			} else {
				
				$this->isAllowed($it);
				
			}
			
		}
		
	}

}


/**
 * AddService class
 *
 * @package default
 */
class AddService extends Serve {

}



/**
 * PasswordPolicy class
 *
 * @package default
 */
class PasswordPolicy extends Service{

	public $policy;

	public function add($policy){
		
		$AddService = new AddService($policy, __FUNCTION__);

		$this -> addService($AddService);		
		
	}

	public function start($exculsions){
		
		$StartService = new StartServiceExculsions($this->items,$exculsions);

	}

}
?>
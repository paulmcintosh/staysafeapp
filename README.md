### READ ME ###

##Technical Exercise##

In order to manage password policies, we use a package which we have developed and
open sourced at: https://github.com/staysafe/password-policy-builder
What changes would you make in order for it to be able to:
1. Be run in a PHP 7.4 and a PHP 8.0 environment?
2. Extend the package to allow exclusion lists with words or characters that should not
be allowed?
3. What tests would you add to ensure the new functionality works as expected?
4. Which additional tools or automation would you use to make the package more
robust?
For this exercise, you can either
1. Describe what changes you think are required to be made and where; or
2. Make code changes and send us a link or zip file with the code.

# Description

1. The changes I've implemented for the PHP 7.4 and a PHP 8.0 environment is the addition 
of the Standard PHP Library in regard to the use of Iterators. This is enables functionality to
be handled by the Array Iterator, Caching Iterator,etc. This will enable the scalability of the application
inregards of future development

2. The Services Interface is location of the isAllowed() method, which is implemented
in the Serve class. This will allow for exculsion to be processed as required

3. Create a unit test on the relevant object methods. Using PHPUnit to conduct assertions()
```
	public function testAddService(){
			
	}
```

4. The use of SplQueue to enqueue tasks of processing the exculsion list can can be resource hungry.
Or possibly the yield functionality. Automation tools RabbitMQ, Ansible, etc



```
	$pass = new PasswordPolicy();
				
	$passwords = array(
		'foo',
		'bar',
		'hello'
	);
	
	$pass->add($passwords);
	
	$exculsions = array(
		'hello',
		'test'
	);
							
	$pass->start($exculsions);
	
```